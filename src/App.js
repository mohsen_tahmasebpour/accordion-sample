import React from "react";
import Accordation from "./components/accordation/accordation";

function App() {
  const acc = [
    {
      title: "Section 1",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit",
      id: 1,
    },
    {
      title: "Section 2",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit",
      id: 2,
    },
    {
      title: "Section 3",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit",
      id: 3,
    },
    {
      title: "Section 4",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit",
      id: 4,
    },
  ];
  return (
    <div>
      <Accordation acc={acc} />
    </div>
  );
}

export default App;
