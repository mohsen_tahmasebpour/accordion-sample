import React from "react";
import AccordionItems from "./accordionItems";

export default class Accordation extends React.Component {
  render() {
    const { acc } = this.props;
    return (
      <div>
        {acc.map((item) => (
          <div key={item.id}>
            <AccordionItems item={item} />
          </div>
        ))}
      </div>
    );
  }
}
