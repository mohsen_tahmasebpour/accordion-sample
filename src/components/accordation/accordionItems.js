import React from "react";

export default class AccordionItems extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  render() {
    const { item } = this.props;
    const { open } = this.state;
    return (
      <div>
        <button
          onClick={() => this.setState({ open: !this.state.open })}
          className={`accordion ${open && "active"}`}
        >
          {item.title}
        </button>
        <div className="panel">{open && <p>{item.description}</p>}</div>
      </div>
    );
  }
}
